# Data Respons build system for Cargotec


## How To Use

External recipe sources are included in the project as git submodules.
These modules need to be initialized on first use:

## Adopt to Business Lines


Basic SDAF is available in meta-sdaf while basic Remion is in meta-hiab

For instance for Bromma, add meta-bromma and meta-qt4 layers and put image recipes in meta-bromma.

Start of by initializing git


`$ git submodule update --init`

OpenEmbedded (BitBake) relies on the build environment providing a large
set of environment variables.  These are most easily set up by sourcing
the provide 'env' script:

`$ . ./env`

This will generate artifacts under the build sub-folder.
If you need more flexibility in where to build do:

```
$ export TEMPLATECONF=<top of project>/build/conf
$ export DR_CM_COMMIT=`git -C <top of project> describe --tags`
$ export DR_BUILD_PLAN=<name of yout build>
$ export DR_BUILD_NO=<yout build number if needed>
$ export BB_ENV_EXTRAWHITE="DR_BUILD_PLAN DR_BUILD_NO DR_CM_COMMIT"
$ source <top of project>/oe-core/oe-init-build-env build <top of project>/bitbake/
```
And now you can build for SDAF:

```
$ DISTRO=sdaf-distro bitbake sdaf-image
$ DISTRO=sdaf-distro-dev bitbake sdaf-dev-image
```

for HIAB

```
$ DISTRO=hiab-distro bitbake hiab-image
$ DISTRO=hiab-distro-dev bitbake hiab-dev-image
```
## How This Differs From Original


- Use oe-core instead of poky.  Poky is actually just oe-core plus
  two other kernel/bsp repos mashed together.  Since the Cargotec
  project doesn't use anything from these other repos, Poky brings
  nothing that oe-core doesn't already have.  Better to use oe-core
  as that's the open source project whereas Poky is Intel's monster...
