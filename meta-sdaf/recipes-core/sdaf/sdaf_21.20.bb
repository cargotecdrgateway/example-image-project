DESCRIPTION="A systemd generator that creates units for all SDAF \
processes in /app"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit systemd

SRC_URI="\
	file://sdaf.service.in \
	file://sdaf.sh \
"

APPFILE = "application_${PV}.tgz"
FETCHCMD_wget += "--password=cargotec17"
SRC_URI[md5sum] = "fa9d2359b138ea7907ccc53a1c855dbf"
SRC_URI[sha256sum] = "d82d46fc2c9893d3f487160b0515727134fc594360c332c02c4cffe1ea0bfa59"
SRC_URI += "ftp://Cargotec@ftp1.datarespons.com/Cargotec/TCS/SDAF_PILOT/${APPFILE}"
SYSTEMD_SERVICE_${PN} = "sdaf.service"

PACKAGES =+ "${PN}-apps"

RDEPENDS_${PN} += "nvram ct-dbus-session platformd"
RDEPENDS_${PN}-apps += "libdbus-c++ dbus-glib libssl libcrypto util-linux-libuuid libgps"
INSANE_SKIP_${PN}-apps += "already-stripped build-deps dev-so file-rdeps"
WARN_QA_remove = "already-stripped" 

FILES_${PN} += "\
	${systemd_unitdir}/system-generators/* \
	${systemd_unitdir}/system/* \
	${datadir}/sdaf/* \
"

FILES_${PN}-apps = "/app/*"

do_unpack_app () {
	mkdir -p ${WORKDIR}/app
	tar -C ${WORKDIR}/app -xf ${DL_DIR}/${APPFILE}
	for f in ${WORKDIR}/app/*.sh;
		do sed -i 's/bash/sh/g' "$f";
	done
}

python do_unpack_append() {
    bb.build.exec_func('do_unpack_app', d)
}

do_compile () {
	sed 's:@bindir@:${bindir}:g' < ${WORKDIR}/sdaf.service.in > ${WORKDIR}/sdaf.service
}

do_install () {
	install -d ${D}${datadir}/sdaf
	install -m 0755 ${WORKDIR}/sdaf.sh ${D}${datadir}/sdaf/dummy-application-manager.sh
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/sdaf.service ${D}${systemd_unitdir}/system/
	install -d ${D}/app
	cp -r ${WORKDIR}/app/* ${D}/app
}
