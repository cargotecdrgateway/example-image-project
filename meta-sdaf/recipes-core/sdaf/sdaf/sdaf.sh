#!/bin/sh

# This is a dummy application that should be replaced with the real SDAF
# application when it's available

echo "SDAF started"

while true; do
	busctl call com.dr.platform.system /com/dr/platform/system/watchdog com.dr.platform.system.watchdog Kick
	sleep 5
done
