DESCRIPTION = "SDAF Development Image"

require sdaf-image.bb

FEATURE_PACKAGES_sdaf-dev-apps = "\
	packagegroup-cargotec-dev-extra \
	packagegroup-cargotec-super-dev-extra \
"

IMAGE_FEATURES += "sdaf-dev-apps empty-root-password"
