SUMMARY = "Cargotec Remion items"
DESCRIPTION = "Generate builtin users and configuration"
SECTION = "base"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
	file://hiab-remote-client.service.in \
	file://disableusb.sh \
	file://70-ttymxc.rules \
	file://11-led.rules \
	file://12-smc.rules \
	file://hiab-check-perms.sh \
	file://hiab-user-check.service.in \
"

S = "${WORKDIR}"

APPFILE = "application-1.3.0.tar.bz2"

inherit useradd systemd

SRC_URI += "ftp://Cargotec@ftp1.datarespons.com/Cargotec/Remion/${APPFILE}"
FETCHCMD_wget += "--password=cargotec17"
SRC_URI[md5sum] = "ad1db925b4d2b92586e9d6809f43fa6f"
SRC_URI[sha256sum] = "28b458e30905715de7dbc946fba9d7336f826988877c4a75d617a16f5f404a71"

PACKAGES =+ "${PN}-apps"

RDEPENDS_${PN} += "nvram platformd sudo"
INSANE_SKIP_${PN}-apps += "already-stripped build-deps dev-so file-rdeps"
WARN_QA_remove = "already-stripped" 

FILES_${PN} += "\
	${systemd_unitdir}/system/* \
"

FILES_${PN}-apps = "/app/*"

do_unpack_app () {
	mkdir -p ${WORKDIR}/app
	rm -rf ${WORKDIR}/app/*
	tar -C ${WORKDIR}/app -xf ${DL_DIR}/${APPFILE}
}

python do_unpack_append() {
    bb.build.exec_func('do_unpack_app', d)
}

USERADD_PACKAGES = "${PN}"
GROUPADD_PARAM_${PN} = "-g 1201 remion"
USERADD_PARAM_${PN} = "--uid 1201 --gid remion --create-home --shell /bin/bash --groups tty,input,dialout,users,adm --system remion"

do_configure () {
}

do_compile () {
	sed 's:@bindir@:${bindir}:g' < ${WORKDIR}/hiab-remote-client.service.in > ${WORKDIR}/hiab-remote-client.service
	sed 's:@bindir@:${bindir}:g' < ${WORKDIR}/hiab-user-check.service.in > ${WORKDIR}/hiab-user-check.service
}

do_install () {
    
    install -d ${D}${systemd_unitdir}/system
    install -m 0644 ${WORKDIR}/hiab-remote-client.service ${D}${systemd_unitdir}/system/
    install -m 0644 ${WORKDIR}/hiab-user-check.service ${D}${systemd_unitdir}/system/
    install -d ${D}${bindir}
    install -m 0755 ${WORKDIR}/disableusb.sh ${D}${bindir}/
    install -m 0755 ${WORKDIR}/hiab-check-perms.sh ${D}${bindir}/
    
    install -d ${D}${sysconfdir}/udev/rules.d
	install -m 0644 ${WORKDIR}/70-ttymxc.rules ${D}${sysconfdir}/udev/rules.d/
	
	install -d -m 0775 -g adm ${D}/app
	cp -r ${WORKDIR}/app/* ${D}/app
	chown -R root:adm ${D}/app
	chmod -R g+w ${D}/app
	
	install -d ${D}${sysconfdir}/udev/rules.d
	install -m 0644 ${WORKDIR}/11-led.rules ${D}${sysconfdir}/udev/rules.d
	install -m 0644 ${WORKDIR}/12-smc.rules ${D}${sysconfdir}/udev/rules.d
	
}

FILES_${PN} = "${bindir}/ ${sysconfdir}/udev/rules.d/ /home/remion/ \
	${systemd_unitdir}/system/* \
"

INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE_${PN} = "hiab-remote-client.service hiab-user-check.service"
