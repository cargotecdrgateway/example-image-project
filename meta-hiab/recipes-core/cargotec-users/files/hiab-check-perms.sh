#!/bin/sh

cp=/appdata/data
if [ -d "$cp" ]; then
	owner=`stat -c %U $cp`
	if [ "$owner" != "remion" ]; then 
		echo "Taking ownership of $cp"
		chown -R remion:adm /appdata/data
		echo "Setting group write permissions"
		find $cp -executable -exec chmod g+rwx '{}' \;
		find $cp -type f -exec chmod g+rw '{}' \;
	fi
fi
