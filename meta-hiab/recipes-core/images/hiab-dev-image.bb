DESCRIPTION = "Cargotec Dev Image"

require hiab-image.bb

FEATURE_PACKAGES_hiab-dev-apps = "\
	packagegroup-cargotec-dev-extra \
	packagegroup-cargotec-super-dev-extra \
"

IMAGE_FEATURES += "hiab-dev-apps empty-root-password"
