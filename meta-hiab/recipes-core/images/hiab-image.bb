DESCRIPTION = "Cargotec Image"

FEATURE_PACKAGES_hiab-apps = "\
	packagegroup-cargotec-base \
	packagegroup-wl18xx \
"

IMAGE_FEATURES = "hiab-apps package-management ssh-server-openssh post-install-logging"
IMAGE_INSTALL_append = " cargotec-users cargotec-users-apps"

inherit core-image dr-image-info

ROOT_KEY = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDOvBzQwdLPtQa26fkNCabGIuC1PjRR1vexvyz6Uc/EYpmbHzlRhZ7m+YNSCJIfUwgIcaGUTp1OrMtTuBo/1cB2Z9jtG0ksoGMfcWLS6RUbaSLu8uLMg9/qTHNANkQe5fMEnKfOmhZ3Mss6mn1MpNg7uKiXmBEfa/QB4hqko/Y7U9irrGC0UUca+c0c83Sr47Y320faGBJV/bYjFk8wHKXAwXK7Po6Tg1sAvDUVL5PTAIPrFV0xKtERpDT0t6ygxnPTuZEAZNkN1vEDvVRzSH30BwMRbTwONUxDHtXXfN2bItr6f+L9XZMH2yTCMO9m+fhFiv82topL0ckGrG5BRDId"

#DEPENDS += "${INITRAMFS_IMAGE}"
#do_rootfs[depends] += "${INITRAMFS_IMAGE}:do_image_complete"

ROOTFS_POSTPROCESS_COMMAND_append = " \
	add_ssh_root_key; \
	inhibit_powerkey_shutdown; \
	add_mounts; \
	${@bb.utils.contains('DISTRO_FEATURES', 'harden', ' set_root_passwd;', '', d)} \
"

install_initrd () {
    install -d ${IMAGE_ROOTFS}/boot
    install -m 0644 ${DEPLOY_DIR_IMAGE}/${INITRAMFS_IMAGE}-${MACHINE}.cpio.gz.u-boot ${IMAGE_ROOTFS}/boot/initrd
}

add_ssh_root_key () {
    install -d -m 700 ${IMAGE_ROOTFS}/home/root/.ssh
    echo ${ROOT_KEY} > ${IMAGE_ROOTFS}/home/root/.ssh/authorized_keys
    chmod 0600  ${IMAGE_ROOTFS}/home/root/.ssh/authorized_keys
}


inhibit_powerkey_shutdown () {
    echo "HandlePowerKey=ignore" >> ${IMAGE_ROOTFS}/etc/systemd/logind.conf
}

set_root_passwd() {
   sed 's%^root:[^:]*:%root:$6$qpQtmIRTJdMgugZp$oP6NZaSFUEAe.jUVywDA9wbqhYj8YCZ8pbWe2DzhPxl5jM/oTzeplj7Gq9CvI21hTzVDnFm7RcAx8IISZjEYr1:%' \
       < ${IMAGE_ROOTFS}/etc/shadow \
       > ${IMAGE_ROOTFS}/etc/shadow.new;
   mv ${IMAGE_ROOTFS}/etc/shadow.new ${IMAGE_ROOTFS}/etc/shadow ;
}

connman_restrict () {
	mkdir -p ${IMAGE_ROOTFS}/etc/connman
	echo "[General]" > ${IMAGE_ROOTFS}/etc/connman/main.conf
	echo "DefaultAutoConnectTechnologies=" >> ${IMAGE_ROOTFS}/etc/connman/main.conf
	echo "EnableOnlineCheck=false" >> ${IMAGE_ROOTFS}/etc/connman/main.conf
}

add_mounts () {
	mkdir -p ${IMAGE_ROOTFS}/appdata
	mkdir -p ${IMAGE_ROOTFS}/service
	echo "/dev/mmcblk0p3 /appdata auto defaults 0 2" >> ${IMAGE_ROOTFS}/etc/fstab
	echo "/dev/mmcblk0p4 /service auto defaults 0 2" >> ${IMAGE_ROOTFS}/etc/fstab
}
